# EASY

# Write a method that returns the range of its argument (an array of integers).
def range(arr)
  # your code goes here
  (arr.sort.first - arr.sort.last).abs
end

# Write a method that returns a boolean indicating whether an array is in sorted
# order. Use the equality operator (==), which returns a boolean indicating
# whether its operands are equal, e.g., 2 == 2 => true, ["cat", "dog"] ==
# ["dog", "cat"] => false
def in_order?(arr)
  # your code goes here
  arr == arr.sort
end


# MEDIUM

# Write a method that returns the number of vowels in its argument
def num_vowels(str)
  # your code goes here
  vowels = "aeiouAEIOU"
  only_vowels = str.split("").select do |ch|
    vowels.include?(ch)
  end
  only_vowels.length
end

# Write a method that returns its argument with all its vowels removed.
def devowel(str)
  # your code goes here
  vowels = "aeiouAEIOU"
  result = str.split("").select do |ch|
    !vowels.include?(ch)
  end
  result.join
end


# HARD

# Write a method that returns the returns an array of the digits of a
# non-negative integer in descending order and as strings, e.g.,
# descending_digits(4291) #=> ["9", "4", "2", "1"]
def descending_digits(int)
  # your code goes here
  int.to_s.split("").sort.reverse
end

# Write a method that returns a boolean indicating whether a string has
# repeating letters. Capital letters count as repeats of lowercase ones, e.g.,
# repeating_letters?("Aa") => true
def repeating_letters?(str)
  # your code goes here
  str.downcase.split("") != str.downcase.split("").uniq
end

# Write a method that converts an array of ten integers into a phone number in
# the format "(123) 456-7890".
def to_phone_number(arr)
  # your code goes here
  chunk_one = arr[0..2].join
  chunk_two = arr[3..5].join
  chunk_three = arr[6..9].join
  result = "(#{chunk_one}) #{chunk_two}-#{chunk_three}"
  result
end

# Write a method that returns the range of a string of comma-separated integers,
# e.g., str_range("4,1,8") #=> 7
def str_range(str)
  # your code goes here
  (str.split(",").sort.first.to_i - str.split(",").sort.last.to_i).abs
end


#EXPERT

# Write a method that is functionally equivalent to the rotate(offset) method of
# arrays. offset=1 ensures that the value of offset is 1 if no argument is
# provided. HINT: use the take(num) and drop(num) methods. You won't need much
# code, but the solution is tricky!
def my_rotate(arr, offset=1)
  # your code goes here
  off_mod = offset.abs % arr.length
  if offset >= 0
    return arr.drop(off_mod) + arr.take(off_mod)
  else
    off_mod = arr.length - off_mod
    return arr.drop(off_mod) + arr.take(off_mod)
  end
end
